#!/bin/bash

dirs="app"

for d in $dirs ; do
  IMAGE_TAG="$CI_REGISTRY_IMAGE/$d:$CI_COMMIT_REF_SLUG"
  (cd $d && docker build -t $IMAGE_TAG . && docker push $IMAGE_TAG)
done

