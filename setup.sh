#!/bin/bash

git config --local core.hooksPath .githooks
npm config set scripts-prepend-node-path true

(cd app && yarn)
yarn

echo "🦕 Configs setup correctly"
