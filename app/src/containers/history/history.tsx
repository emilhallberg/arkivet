import React, { useState } from 'react';
import styled from 'styled-components';
import { AnimatePresence, motion } from 'framer-motion';
import Landing from './components/landing';
import Episode from './components/episode';
import { HistoryContext } from './context';
import Controls from './components/controls';
import { p } from '../../utils/library';
import { episodes } from '../../data/episodes';

const SHistory = styled(motion.div)`
  display: grid;
  grid-auto-columns: 100vw;
  grid-auto-flow: column;
  min-height: 100vh;
  position: relative;
`;

const History: React.FC = () => {
  const [[page, direction], setPage] = useState([0, 0]);
  const [start, haveStarted] = useState(false);

  const paginate = (d: number) => {
    const v = page + d;
    if (v < 0) {
      haveStarted(false);
      return d;
    }
    if (v > episodes.length - 1) return d;
    setPage([v, d]);
    return d;
  };

  const goToPage = (i: number) => {
    setPage([i, i < page ? -1 : 1]);
    return i;
  };

  const value = {
    page,
    direction,
    length: episodes.length,
    paginate,
    goToPage,
  };

  if (!start) return <Landing onClick={() => haveStarted(true)} />;

  return (
    <HistoryContext.Provider value={value}>
      <SHistory>
        <AnimatePresence initial={false} custom={direction}>
          <Episode>
            <Episode.Title>{`${episodes[page].year} -`}</Episode.Title>
            <Episode.SubTitle>{episodes[page].title}</Episode.SubTitle>
            <Episode.Text>{p(episodes[page].text)}</Episode.Text>
            <Episode.Images data={episodes[page].images} />
          </Episode>
        </AnimatePresence>
        <Controls />
      </SHistory>
    </HistoryContext.Provider>
  );
};

export default History;
