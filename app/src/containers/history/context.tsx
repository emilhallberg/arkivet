import { createContext } from 'react';

// eslint-disable-next-line import/prefer-default-export
export const HistoryContext = createContext({
  page: 0,
  direction: 0,
  length: 0,
  paginate: (d: number): number => d,
  goToPage: (i: number): number => i,
});
