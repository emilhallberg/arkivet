import React, { useContext } from 'react';
import styled from 'styled-components';
import { motion } from 'framer-motion';
import { HistoryContext } from '../context';
import Images from './images';
import Title from './title';
import Text from './text';
import media from '../../../resources/stylesheets/media';
import SubTitle from './subTitle';

const SEpisode = styled(motion.div)`
  position: absolute;
  height: 100%;
  width: 100%;
  display: grid;
  grid-template-columns: 1fr 2fr 2fr 1fr;
  grid-template-rows: max-content max-content 1fr;
  grid-template-areas: '. title title .' '. subTitle images .' '. text images .';
  grid-gap: 16px 128px;
  justify-content: center;
  box-sizing: border-box;
  padding: 10vh 16px 0 16px;
  @media (${media.phone}) {
    grid-gap: 32px;
    grid-template-columns: 1fr;
    grid-template-rows: max-content min-content min-content max-content;
    grid-template-areas: 'title' 'subTitle' 'text' 'images';
    overflow-y: auto;
  }
`;

const variants = {
  enter: (direction: number) => ({
    zIndex: 1,
    x: direction > 0 ? '100vw' : '-100vw',
    opacity: 0,
  }),
  center: {
    zIndex: 1,
    x: 0,
    opacity: 1,
  },
  exit: (direction: number) => ({
    zIndex: 0,
    x: direction < 0 ? '100vw' : '-100vw',
    opacity: 0,
  }),
};

const transition = {
  x: { type: 'spring', stiffness: 300, damping: 200, duration: 1 },
  opacity: { duration: 0.2 },
};

const swipeConfidenceThreshold = 10000;
const swipePower = (offset: number, velocity: number) => {
  return Math.abs(offset) * velocity;
};

interface EpisodeComp<T> extends React.FC<T> {
  Title: typeof Title;
  SubTitle: typeof SubTitle;
  Text: typeof Text;
  Images: typeof Images;
}

interface EpisodeProps {
  children: React.ReactNode;
}

const Episode: EpisodeComp<EpisodeProps> = ({ children }: EpisodeProps) => {
  const { page, direction, paginate } = useContext(HistoryContext);

  return (
    <SEpisode
      key={page}
      custom={direction}
      variants={variants}
      initial="enter"
      animate="center"
      exit="exit"
      transition={transition}
      whileTap={{ cursor: 'grabbing' }}
      drag="x"
      dragConstraints={{ left: 0, right: 0 }}
      dragElastic={1}
      onDragEnd={(e, { offset, velocity }) => {
        const swipe = swipePower(offset.x, velocity.x);
        if (swipe < -swipeConfidenceThreshold) {
          paginate(1);
        } else if (swipe > swipeConfidenceThreshold) {
          paginate(-1);
        }
      }}
    >
      {children}
    </SEpisode>
  );
};

Episode.Title = Title;
Episode.SubTitle = SubTitle;
Episode.Text = Text;
Episode.Images = Images;

export default Episode;
