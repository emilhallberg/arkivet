import React, { useContext } from 'react';
import styled from 'styled-components';
import { motion } from 'framer-motion';
import Colors from '../../../resources/stylesheets/colors';
import { HistoryContext } from '../context';
import { hexa } from '../../../utils/library';
import { episodes } from '../../../data/episodes';
import media from '../../../resources/stylesheets/media';

const STick = styled(motion.div)`
  position: absolute;
  height: 12px;
  width: 12px;
  border-radius: 50%;
  background: ${Colors.text};
`;

const SLabel = styled(motion.h6)`
  position: absolute;
  top: -38px;
  left: 0;
  transform: rotate(-58deg);
  color: ${Colors.text};
  cursor: pointer;
  @media (${media.phone}) {
    display: none;
  }
`;

const transition = {
  scale: { type: 'spring', stiffness: 200, damping: 200, duration: 0.2 },
};

interface TickProps {
  position: number;
}

const Tick: React.FC<TickProps> = ({ position }: TickProps) => {
  const { length, page, goToPage } = useContext(HistoryContext);
  const active = position === page || position < page + 1;

  return (
    <STick
      whileHover={{
        scale: 1.2,
        background: Colors.text,
      }}
      whileTap={{ scale: 0.95 }}
      style={{ left: `calc(${(position / (length - 1)) * 100}% - 6px)` }}
      animate={{
        scale: active ? 1.2 : 1,
        background: active ? Colors.text : hexa(Colors.text, -100),
      }}
      transition={transition}
      onClick={() => goToPage(position)}
    >
      <SLabel>{episodes[position].year}</SLabel>
    </STick>
  );
};

export default Tick;
