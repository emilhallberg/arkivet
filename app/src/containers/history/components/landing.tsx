import React from 'react';
import styled from 'styled-components';
import { motion } from 'framer-motion';
import Logo from '../../../resources/assets/logo';
import Colors from '../../../resources/stylesheets/colors';
import Button from '../../../components/button/button';
import Click from '../../../components/button/click';
import media from '../../../resources/stylesheets/media';

const SLanding = styled(motion.div)`
  display: grid;
  grid-template-columns: 1fr 1fr 1fr;
  grid-template-rows: 1fr 3fr max-content 1fr;
  grid-template-areas: '. . .' '. logo .' '. button .' '. . .';
  grid-gap: 64px;
  justify-items: center;
  height: 100%;
  width: 100%;
  @media (${media.phone}) {
    grid-gap: 16px;
    grid-template-columns: 16px 1fr 16px;
  }
`;

const SLogo = styled(Logo)`
  grid-area: logo;
  color: ${Colors.text};
`;

const SClick = styled(Click)`
  grid-area: button;
`;

interface LandingProps {
  onClick: React.EventHandler<React.SyntheticEvent>;
}

const Landing: React.FC<LandingProps> = ({ onClick }: LandingProps) => {
  return (
    <SLanding>
      <SLogo />
      <SClick onAction={onClick}>
        <Button>Start</Button>
      </SClick>
    </SLanding>
  );
};

export default Landing;
