import React from 'react';
import styled from 'styled-components';
import { motion } from 'framer-motion';
import media from '../../../resources/stylesheets/media';
import Colors from '../../../resources/stylesheets/colors';

const SText = styled(motion.div)`
  grid-area: text;
  display: grid;
  grid-auto-rows: max-content;
  grid-gap: 16px;
  margin-left: 48px;
  color: ${Colors.text};
  @media (${media.phone}) {
    margin-left: 0;
  }
`;

interface TextProps {
  children: React.ReactNode;
}

const Text: React.FC<TextProps> = ({ children }: TextProps) => {
  return (
    <SText
      animate={{
        y: [100, 0],
        opacity: [0, 1],
      }}
    >
      {children}
    </SText>
  );
};

export default Text;
