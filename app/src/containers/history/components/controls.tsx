import React, { useContext } from 'react';
import styled from 'styled-components';
import { motion } from 'framer-motion';
import Click from '../../../components/button/click';
import Button from '../../../components/button/button';
import { HistoryContext } from '../context';
import Timeline from './timeline';
import media from '../../../resources/stylesheets/media';
import Colors from '../../../resources/stylesheets/colors';

const SControls = styled(motion.div)`
  position: absolute;
  bottom: 16px;
  left: 16px;
  right: 16px;
  display: grid;
  grid-template-columns: 128px 1fr 128px;
  grid-template-areas: 'back timeline next';
  z-index: 10;
  align-items: center;
  grid-gap: 64px;
  @media (${media.phone}) {
    background-color: ${Colors.background};
    bottom: 0;
    left: 0;
    right: 0;
    padding: 16px;
    grid-gap: 16px;
    grid-template-columns: 1fr;
    grid-template-rows: 32px;
    grid-template-areas: 'timeline';
  }
`;

const SBack = styled(Click)`
  grid-area: back;
  @media (${media.phone}) {
    display: none;
  }
`;

const SNext = styled(Click)`
  grid-area: next;
  @media (${media.phone}) {
    display: none;
  }
`;

const Controls: React.FC = () => {
  const { paginate } = useContext(HistoryContext);
  return (
    <SControls>
      <SBack onAction={() => paginate(-1)}>
        <Button>Tillbaka</Button>
      </SBack>
      <Timeline />
      <SNext onAction={() => paginate(1)}>
        <Button>Nästa</Button>
      </SNext>
    </SControls>
  );
};

export default Controls;
