import React, { useState } from 'react';
import styled from 'styled-components';
import { motion } from 'framer-motion';
import media from '../../../resources/stylesheets/media';

const SCard = styled(motion.div)`
  margin: 0 24px;
  position: relative;
  @media (${media.phone}) {
    margin: 0;
  }
`;

const SImage = styled(motion.img)`
  width: 100%;
  height: 100%;
  object-fit: cover;
  border-radius: 8px;
`;

const SText = styled(motion.h4)`
  position: absolute;
  top: calc(50% - 25px);
  left: 0;
  right: 0;
  text-align: center;
  height: 50px;
`;

interface CardProps {
  src: string;
  alt: string;
}

const Card: React.FC<CardProps> = ({ src, alt }: CardProps) => {
  const [open, isOpen] = useState(false);
  return (
    <SCard layoutId={src} onClick={() => isOpen((v) => !v)}>
      <SImage
        src={src}
        alt={alt}
        animate={{
          opacity: open ? [1, 0.3] : [0.3, 1],
          scale: [0.8, 1],
          y: [100, 0],
        }}
      />
      {open && <SText>{alt}</SText>}
    </SCard>
  );
};

export default Card;
