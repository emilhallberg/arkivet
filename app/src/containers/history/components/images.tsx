import React from 'react';
import styled from 'styled-components';
import { AnimateSharedLayout, motion } from 'framer-motion';
import Card from './card';
import Image from './image';

const SImages = styled(motion.div)`
  grid-area: images;
  display: grid;
  grid-template-columns: 1fr;
  grid-gap: 32px;
  overflow-y: auto;
  &::after {
    content: '';
    padding-bottom: 120px;
  }
`;

export interface Image {
  id: string;
  src: string;
  alt: string;
}

export interface ImagesProps {
  data: Image[];
}

const Images: React.FC<ImagesProps> = ({ data }: ImagesProps) => {
  return (
    <AnimateSharedLayout type="crossfade">
      <SImages layout>
        {data.map(({ src, alt, id }) => (
          <Card key={id} src={src} alt={alt} />
        ))}
      </SImages>
    </AnimateSharedLayout>
  );
};

export default Images;
