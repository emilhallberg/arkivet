import React from 'react';
import styled from 'styled-components';
import { motion } from 'framer-motion';
import { hexo } from '../../../utils/library';
import Colors from '../../../resources/stylesheets/colors';

const SImage = styled(motion.div)`
  top: 0;
  left: 0;
  right: 0;
  bottom: 0;
  position: fixed;
  z-index: 20;
  overflow: hidden;
  padding: 40px 0;
  display: grid;
  align-items: center;
  justify-content: center;
  background: ${hexo(Colors.background, 0.5)};
`;

const SContent = styled(motion.img)`
  height: 50%;
  width: auto;
`;

interface ImageProps {
  children: React.ReactNode;
  src: string;
  alt: string;
  layoutId: string;
}

const Image: React.FC<ImageProps> = ({
  src,
  alt,
  children,
  layoutId,
}: ImageProps) => {
  return (
    <SImage
      layoutId={layoutId}
      initial={{ opacity: 0 }}
      animate={{ opacity: 1 }}
      exit={{ opacity: 0, transition: { duration: 0.15 } }}
      transition={{ duration: 0.2, delay: 0.15 }}
      style={{ pointerEvents: 'auto' }}
    >
      <SContent src={src} alt={alt} />
      {children}
    </SImage>
  );
};

export default Image;
