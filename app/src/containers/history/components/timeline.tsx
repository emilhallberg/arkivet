import React, { useContext } from 'react';
import styled from 'styled-components';
import { motion } from 'framer-motion';
import { HistoryContext } from '../context';
import { hexa } from '../../../utils/library';
import Colors from '../../../resources/stylesheets/colors';
import Tick from './tick';

const SLine = styled(motion.div)`
  grid-area: timeline;
  background: ${hexa(Colors.text, -100)};
  z-index: 10;
  position: relative;
  display: grid;
  align-items: center;
`;

const STime = styled(motion.div)`
  height: 4px;
  background: ${Colors.text};
`;

const Timeline: React.FC = () => {
  const { page, length } = useContext(HistoryContext);
  const width = `${(page / (length - 1)) * 100}%`;

  return (
    <SLine>
      <STime
        animate={{ width }}
        transition={{
          width: { type: 'spring', stiffness: 200, damping: 100, duration: 1 },
        }}
      />
      {[...new Array(length)].map((_, i) => (
        <Tick key={i.toString()} position={i} />
      ))}
    </SLine>
  );
};

export default Timeline;
