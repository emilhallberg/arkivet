import React from 'react';
import styled from 'styled-components';
import { motion } from 'framer-motion';
import Fonts from '../../../resources/stylesheets/fonts';

const STitle = styled(motion.h1)`
  grid-area: title;
  text-transform: uppercase;
  font-size: ${Fonts.Size.largeXXXXX};
`;

interface TitleProps {
  children: React.ReactNode;
}

const Title: React.FC<TitleProps> = ({ children }: TitleProps) => (
  <STitle
    animate={{
      x: [200, 0],
    }}
  >
    {children}
  </STitle>
);

export default Title;
