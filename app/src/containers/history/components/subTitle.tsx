import React from 'react';
import styled from 'styled-components';
import { motion } from 'framer-motion';
import media from '../../../resources/stylesheets/media';

const SSubTitle = styled(motion.h3)`
  grid-area: subTitle;
  margin-left: 48px;
  @media (${media.phone}) {
    margin-left: 0;
  }
`;

interface TitleProps {
  children: React.ReactNode;
}

const SubTitle: React.FC<TitleProps> = ({ children }: TitleProps) => (
  <SSubTitle
    animate={{
      x: [200, 0],
    }}
  >
    {children}
  </SSubTitle>
);

export default SubTitle;
