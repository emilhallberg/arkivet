import React, { useState } from 'react';
import styled from 'styled-components';
import { useParams } from 'react-router';
import { motion } from 'framer-motion';
import parse from 'html-react-parser';
import media from '../../resources/stylesheets/media';
import SubTitle from '../history/components/subTitle';
import { cap } from '../../utils/library';
import { getArticles } from '../../data/articles';
import Input from '../../components/form/input';
import Text from '../history/components/text';

const SCategory = styled.div`
  display: grid;
  grid-template-columns: 1fr 3fr 1fr 1fr;
  grid-template-areas: '. subTitle search .' '. text text .' '. articles articles.';
  grid-auto-rows: max-content;
  padding: 128px 0;
  grid-gap: 32px;
  @media (${media.phone}) {
    padding: 32px 16px;
    grid-gap: 32px;
    grid-template-columns: 1fr;
  }
`;

const SInput = styled(Input)`
  grid-area: search;
`;

const SArticles = styled(motion.div)`
  grid-area: articles;
  display: grid;
  grid-template-columns: 1fr;
  grid-auto-rows: max-content;
  grid-gap: 32px;
`;

const SArticle = styled.div`
  display: grid;
  grid-template-areas: 'subTitle' 'text';
  grid-gap: 16px;
`;

const Category = () => {
  const { category } = useParams<{ category: string }>();
  const [searchValue, setSearchValue] = useState('');
  const articles = getArticles(category, searchValue);

  return (
    <SCategory>
      <SubTitle>{cap(category)}</SubTitle>
      <SInput
        type="text"
        name="search"
        label="Sök bland referat..."
        value={searchValue}
        onChange={(e: React.ChangeEvent<HTMLInputElement>) =>
          setSearchValue(e.target.value)
        }
      />
      <SArticles
        animate={{
          x: [200, 0],
        }}
      >
        {articles.map((x) => (
          <SArticle key={x.id}>
            <SubTitle>{`${x.header} | ${x.score}`}</SubTitle>
            <Text>
              {x.text.split('\n').map((v) => (
                <p>{parse(v)}</p>
              ))}
            </Text>
          </SArticle>
        ))}
      </SArticles>
    </SCategory>
  );
};

export default Category;
