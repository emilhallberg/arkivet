import React from 'react';
import styled from 'styled-components';
import { motion } from 'framer-motion';
import media from '../../resources/stylesheets/media';
import SubTitle from '../history/components/subTitle';
import { categories } from '../../data/articles';
import { categories as path } from '../../routes/paths';
import Button from '../../components/button/button';
import Link from '../../components/button/link';

const SArticles = styled.div`
  display: grid;
  grid-template-columns: 1fr 4fr 1fr;
  grid-template-areas: '. title .' '. subTitle .' '. text .' '. categories .';
  grid-auto-rows: max-content;
  padding: 128px 0;
  grid-gap: 32px;
  @media (${media.phone}) {
    padding: 32px 16px;
    grid-gap: 32px;
    grid-template-columns: 1fr;
  }
`;

const SContainer = styled(motion.div)`
  grid-area: categories;
  display: grid;
  grid-template-columns: repeat(3, max-content);
  grid-auto-rows: max-content;
  justify-content: center;
  grid-gap: 32px;
  @media (${media.phone}) {
    padding: 32px 16px;
    grid-gap: 32px;
    grid-template-columns: 1fr;
  }
`;

const SCategory = styled(Link)`
  display: grid;
  grid-template-columns: 325px;
  grid-template-rows: max-content;
  @media (${media.phone}) {
    grid-template-columns: 1fr;
  }
`;

const Categories = () => {
  return (
    <SArticles>
      <SubTitle>Välj ett lag</SubTitle>
      <SContainer
        animate={{
          x: [200, 0],
        }}
      >
        {categories.map((x) => (
          <SCategory key={x} to={`${path.path}/${x.toLowerCase()}`}>
            <Button>{x}</Button>
          </SCategory>
        ))}
      </SContainer>
    </SArticles>
  );
};

export default Categories;
