import React, { Suspense } from 'react';
import ReactDOM from 'react-dom';
import Global from './resources/stylesheets/global';
import Loading from './components/loading';
import App from './app/app';

ReactDOM.render(
  <Suspense fallback={<Loading />}>
    <Global />
    <App />
  </Suspense>,
  document.getElementById('root'),
);
