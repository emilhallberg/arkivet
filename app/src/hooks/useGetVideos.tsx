import { useEffect, useState } from 'react';

interface UseGetVideos {
  data: Record<string, unknown>;
  loading: boolean;
  error: string;
}

const URL =
  'https://www.googleapis.com/youtube/v3/channels?part=snippet%2CcontentDetails%2Cstatistics&id=UCtR_4e5-lt1khbw2C-qdy-A&key=';
const KEY = 'AIzaSyDesOIh0Klb0Nb02aVGO0qz8nWiVRXukX8';

const useGetVideos: () => UseGetVideos = () => {
  const [data, setData] = useState({});
  const [loading, isLoading] = useState(true);
  const [error, setError] = useState<string>('');

  useEffect(() => {
    (async () => {
      isLoading(true);
      const res = await fetch(URL + KEY, {
        headers: {
          Accept: 'application/json',
        },
      });
      if (res.status === 200) {
        setData(res.json());
      } else {
        setError("Couldn't get videos");
      }
      isLoading(false);
    })();
  }, []);

  return { data, loading, error };
};

export default useGetVideos;
