import React, { useEffect, useRef } from 'react';

export interface UseDimensions {
  width: number;
  height: number;
}

export const useDimensions: (
  ref: React.RefObject<HTMLDivElement>,
) => UseDimensions = (ref: React.RefObject<HTMLDivElement>) => {
  const dimensions = useRef<UseDimensions>({ width: 0, height: 0 });

  useEffect(() => {
    if (ref.current) {
      dimensions.current.width = ref.current?.offsetWidth;
      dimensions.current.height = ref.current?.offsetHeight;
    }
  }, [ref]);

  return dimensions.current;
};
