import { uuid } from '../utils/library';

export const home = {
  id: uuid(),
  title: 'Hem',
  path: '/',
  header: true,
};

export const history = {
  id: uuid(),
  title: 'Historia',
  path: '/historia',
  header: true,
};

export const categories = {
  id: uuid(),
  title: 'Referat',
  path: '/referat',
  header: true,
};

export const category = {
  id: uuid(),
  title: 'Lag',
  path: '/referat/:category',
  header: false,
};

export const images = {
  id: uuid(),
  title: 'Bilder',
  path: '/bilder',
  header: true,
};

export const videos = {
  id: uuid(),
  title: 'Videos',
  path: '/videos',
  header: true,
};
