import React from 'react';
import { home, history, videos, categories, category, images } from './paths';
import History from '../containers/history/history';
import Videos from '../containers/videos/videos';
import Categories from '../containers/categories/categories';
import Category from '../containers/category/category';

const routes = [
  { ...home, container: <h1>Hem</h1> },
  { ...history, container: <History /> },
  { ...categories, container: <Categories /> },
  { ...category, container: <Category /> },
  { ...images, container: <h1>Bilder</h1> },
  { ...videos, container: <Videos /> },
];

export default routes;
