import React from 'react';
import styled from 'styled-components';

const SEmpty = styled.div`
  display: grid;
  grid-auto-rows: max-content;
  align-content: center;
`;

const Empty: React.FC = () => {
  return (
    <SEmpty>
      <h1>Empty</h1>
    </SEmpty>
  );
};

export default Empty;
