import { v4 } from 'uuid';
import { Image } from '../containers/history/components/images';

export interface Episode {
  id: string;
  year: number;
  title: string;
  text: string;
  images: Image[];
}

const episode: Episode = {
  id: v4(),
  year: 1921,
  title: 'Lorem ipsum dolor sit amet',
  text: 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec quam felis, ultricies nec, pellentesque eu, pretium quis, sem.\n Nulla consequat massa quis enim. Donec pede justo, fringilla vel, aliquet nec, vulputate eget, arcu. In enim justo, rhoncus ut, imperdiet a, venenatis vitae, justo. Nullam dictum felis eu pede mollis pretium. Integer tincidunt. Cras dapibus. Vivamus elementum semper nisi. Aenean vulputate eleifend tellus. Aenean leo ligula, porttitor eu, consequat vitae, eleifend ac, enim. Aliquam lorem ante, dapibus in, viverra quis, feugiat.',
  images: [
    { id: v4(), src: '/images/example-1.jpg', alt: 'Title for image 1' },
    { id: v4(), src: '/images/example-2.jpg', alt: 'Title for image 2' },
    { id: v4(), src: '/images/example-3.jpg', alt: 'Title for image 3' },
  ],
};

export const episodes: Episode[] = [
  { ...episode, id: v4(), year: 1921 },
  { ...episode, id: v4(), year: 1930 },
  { ...episode, id: v4(), year: 1940 },
  { ...episode, id: v4(), year: 1950 },
  { ...episode, id: v4(), year: 1960 },
  { ...episode, id: v4(), year: 1970 },
  { ...episode, id: v4(), year: 1980 },
  { ...episode, id: v4(), year: 1990 },
  { ...episode, id: v4(), year: 2000 },
  { ...episode, id: v4(), year: 2010 },
  { ...episode, id: v4(), year: 2021 },
];
