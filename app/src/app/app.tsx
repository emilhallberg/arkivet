import React from 'react';
import { BrowserRouter } from 'react-router-dom';
import styled from 'styled-components';
import Header from './components/header/header';
import Main from './components/main/main';
import ScrollRestoration from './components/scrollRestoration';

const SApp = styled.div`
  display: grid;
  grid-template-rows: max-content minmax(100vh, max-content);
  grid-template-areas: 'header' 'main';
`;

const App: React.FC = () => {
  return (
    <BrowserRouter>
      <SApp>
        <Header />
        <Main />
      </SApp>
      <ScrollRestoration />
    </BrowserRouter>
  );
};

export default App;
