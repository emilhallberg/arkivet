import * as React from 'react';
import { motion } from 'framer-motion';
import Button from '../../../../components/button/button';
import Link from '../../../../components/button/link';

const variants = {
  open: {
    y: 0,
    opacity: 1,
    transition: {
      y: { stiffness: 1000, velocity: -100 },
    },
  },
  closed: {
    y: -50,
    opacity: 0,
    transition: {
      y: { stiffness: 1000 },
    },
  },
};

interface ItemProps {
  path: string;
  title: string;
}

const Item: React.FC<ItemProps> = ({ path, title }: ItemProps) => {
  return (
    <motion.div variants={variants}>
      <Link to={path}>
        <Button>{title}</Button>
      </Link>
    </motion.div>
  );
};

export default Item;
