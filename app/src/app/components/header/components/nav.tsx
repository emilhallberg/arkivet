import React from 'react';
import { motion } from 'framer-motion';
import styled from 'styled-components';
import Item from './item';
import media from '../../../../resources/stylesheets/media';
import routes from '../../../../routes/routes';

const SNav = styled(motion.nav)`
  display: grid;
  grid-auto-columns: max-content;
  grid-auto-flow: column;
  grid-gap: 16px;
  padding: 0 16px;
  @media (${media.phone}) {
    position: fixed;
    top: 96px;
    right: 16px;
    grid-auto-flow: row;
  }
`;

const variants = {
  open: {
    transition: {
      staggerChildren: 0.04,
      delayChildren: 0.05,
      staggerDirection: -1,
    },
  },
  closed: {
    transition: { staggerChildren: 0.05, staggerDirection: 1 },
  },
};

const Nav: React.FC = () => {
  return (
    <SNav variants={variants}>
      {routes
        .filter(({ header }) => header)
        .map(({ id, path, title }) => (
          <Item key={id} path={path} title={title} />
        ))}
    </SNav>
  );
};

export default Nav;
