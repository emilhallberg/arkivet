import React, { useState } from 'react';
import styled from 'styled-components';
import { motion } from 'framer-motion';
import Nav from './components/nav';
import Hamburger from './components/hamburger';

interface SHeaderProps {
  open: boolean;
}

const SHeader = styled(motion.header)<SHeaderProps>`
  position: fixed;
  right: 80px;
  height: 96px;
  z-index: 2;
  display: grid;
  align-items: center;
`;

const SButton = styled(motion.div)`
  position: fixed;
  right: 16px;
  top: 16px;
  height: 64px;
  width: 64px;
  border-radius: 50%;
  display: grid;
  justify-content: center;
  align-items: center;
`;

const transition = {
  width: { type: 'spring', stiffness: 300, damping: 200, duration: 0.3 },
};

const Header: React.FC = () => {
  const [open, isOpen] = useState(false);
  return (
    <SHeader
      open={open}
      animate={open ? 'open' : 'closed'}
      transition={transition}
      initial={false}
    >
      <Nav />
      <SButton whileHover={{ scale: 1.1 }} whileTap={{ scale: 1 }}>
        <Hamburger toggle={() => isOpen((v) => !v)} open={open} />
      </SButton>
    </SHeader>
  );
};

export default Header;
