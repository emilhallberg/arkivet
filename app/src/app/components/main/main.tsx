import React from 'react';
import { Route, Switch } from 'react-router';
import styled from 'styled-components';
import routes from '../../../routes/routes';
import Empty from '../../../components/empty';

const SMain = styled.main`
  grid-area: main;
  display: grid;
  z-index: 0;
  overflow-y: auto;
`;

const Main: React.FC = () => {
  return (
    <SMain>
      <Switch>
        {routes.map(({ path, container }) => (
          <Route key={path} path={path} exact>
            {container}
          </Route>
        ))}
        <Route>
          <Empty />
        </Route>
      </Switch>
    </SMain>
  );
};

export default Main;
